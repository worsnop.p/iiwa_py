# Installation
  - Follow the iiwa_stack [Setup Guide](https://github.com/SalvoVirga/iiwa_stack/wiki) to install the dependent ROS package.
  - Move to the *src* folder of your catkin workspace and run `git clone https://gitlab.com/worsnop.p/iiwa_py.git`
  - Build your workspace.