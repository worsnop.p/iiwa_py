Building index...
Started generating documentation...
<h1 id="iiwa_py.IIWASubscriber">IIWASubscriber</h1>

```python
IIWASubscriber(self, as_arrays=False, read_pos=True, read_vel=True, read_torque=True, read_pose=True, read_wrench=True, read_stiffness=True, read_damping=True, read_destination=True)
```

Sets up state holders for all the relevant topics of the iiwa and methods to interface with them.

<h2 id="iiwa_py.IIWASubscriber.get_pose">get_pose</h2>

```python
IIWASubscriber.get_pose(self)
```
Returns the current end-effector pose
<h2 id="iiwa_py.IIWASubscriber.get_torque">get_torque</h2>

```python
IIWASubscriber.get_torque(self)
```
Returns the current joint torques
<h2 id="iiwa_py.IIWASubscriber.get_pos">get_pos</h2>

```python
IIWASubscriber.get_pos(self)
```
Returns the current joint positions
<h2 id="iiwa_py.IIWASubscriber.get_wrench">get_wrench</h2>

```python
IIWASubscriber.get_wrench(self)
```
Returns the current end-effector wrench
<h2 id="iiwa_py.IIWASubscriber.stop_recording_states">stop_recording_states</h2>

```python
IIWASubscriber.stop_recording_states(self, *states)
```

Stops recording the states passed and returns their histories
:param states:
:return: A dictionary of the state histories

<h2 id="iiwa_py.IIWASubscriber.get_damping">get_damping</h2>

```python
IIWASubscriber.get_damping(self)
```
Returns the current joint damping
<h2 id="iiwa_py.IIWASubscriber.get_vel">get_vel</h2>

```python
IIWASubscriber.get_vel(self)
```
Returns the current joint velocities
<h2 id="iiwa_py.IIWASubscriber.get_time_of_completion">get_time_of_completion</h2>

```python
IIWASubscriber.get_time_of_completion(self)
```
Returns the last time a command was completed
<h2 id="iiwa_py.IIWASubscriber.get_stiffness">get_stiffness</h2>

```python
IIWASubscriber.get_stiffness(self)
```
Returns the current joint stiffness
<h2 id="iiwa_py.IIWASubscriber.record_states">record_states</h2>

```python
IIWASubscriber.record_states(self, *states)
```

Starts recording the states passed
:param states: each argument in states must be one of ['position', 'velocity', 'torque', 'pose', 'wrench',
'stiffness', 'damping', 'destination']
:return:

<h1 id="iiwa_py.IIWACommander">IIWACommander</h1>

```python
IIWACommander(self)
```

Sets up publishers to all the command topics of the iiwa and provides methods to interface with them

<h2 id="iiwa_py.IIWACommander.transform_cmd">transform_cmd</h2>

```python
IIWACommander.transform_cmd(self, transform)
```
Commands the cartesian pose with transformation matrix
<h2 id="iiwa_py.IIWACommander.vel_cmd">vel_cmd</h2>

```python
IIWACommander.vel_cmd(self, velocities)
```
Commands the joint velocities with an array
<h2 id="iiwa_py.IIWACommander.send_vel_msg">send_vel_msg</h2>

```python
IIWACommander.send_vel_msg(self, msg)
```

Sends a joint velocity command message
:param JointVelocity msg: the command message
:return:

<h2 id="iiwa_py.IIWACommander.send_pos_vel_msg">send_pos_vel_msg</h2>

```python
IIWACommander.send_pos_vel_msg(self, msg)
```

Sends a postion-velocity command message
:param JointPositionVelocity msg: the command message
:return:

<h2 id="iiwa_py.IIWACommander.set_pose">set_pose</h2>

```python
IIWACommander.set_pose(self, position=(0, 0, 0), orientation=(0, 0, 0, 1))
```
Sets the cartesian pose defaulting unspecified components to zero
<h2 id="iiwa_py.IIWACommander.pos_cmd">pos_cmd</h2>

```python
IIWACommander.pos_cmd(self, positions)
```
Commands the joint positions with an array
<h2 id="iiwa_py.IIWACommander.set_joint_positions">set_joint_positions</h2>

```python
IIWACommander.set_joint_positions(self, a1=0.0, a2=0.0, a3=0.0, a4=0.0, a5=0.0, a6=0.0, a7=0.0)
```
Sets the joint positions defaulting unspecified joints to zero
<h2 id="iiwa_py.IIWACommander.set_joint_velocities">set_joint_velocities</h2>

```python
IIWACommander.set_joint_velocities(self, a1=0.0, a2=0.0, a3=0.0, a4=0.0, a5=0.0, a6=0.0, a7=0.0)
```
Sets the joint velocities defaulting unspecified joints to zero
<h2 id="iiwa_py.IIWACommander.send_pos_msg">send_pos_msg</h2>

```python
IIWACommander.send_pos_msg(self, msg)
```

Sends a joint position command message
:param JointPosition msg: the command message
:return:

<h2 id="iiwa_py.IIWACommander.pose_cmd">pose_cmd</h2>

```python
IIWACommander.pose_cmd(self, pose)
```
Commands the cartesian pose with an array
<h2 id="iiwa_py.IIWACommander.pos_vel_cmd">pos_vel_cmd</h2>

```python
IIWACommander.pos_vel_cmd(self, pos_vel)
```

Commands the joint positions and end velocity with either an array
:param pos_vel: can be either [position, velocity] (2, 7) or [position]+[velocity] (14,)
:return:

<h2 id="iiwa_py.IIWACommander.send_pose_msg">send_pose_msg</h2>

```python
IIWACommander.send_pose_msg(self, msg)
```

Sends a cartesian pose command message
:param PoseStamped msg: the command message
:return:

<h1 id="iiwa_py.IIWAInterface">IIWAInterface</h1>

```python
IIWAInterface(self)
```

Inherits boht the IIIWASubscriber and IIWACommander and provides additional methods which takes advantage of both.

<h2 id="iiwa_py.IIWAInterface.zero_joints">zero_joints</h2>

```python
IIWAInterface.zero_joints(self, blocking=True)
```
Sets all joint positions to zero
<h2 id="iiwa_py.IIWAInterface.set_pose">set_pose</h2>

```python
IIWAInterface.set_pose(self, x=None, y=None, z=None, qx=None, qy=None, qz=None, qw=None)
```
Sets the cartesian pose of specific components without changing the others
<h2 id="iiwa_py.IIWAInterface.delta_joint_pos_trajectory">delta_joint_pos_trajectory</h2>

```python
IIWAInterface.delta_joint_pos_trajectory(self, trajectory, times, blocking=True, start_joints=None)
```

Plays a trajectory of joint position commands that are executed at specified times
:param trajectory: Must be iterable providing a 7-length array at each iteration
:param times: an iterable of floats specifying how far from the beginning each point should be executed
:param blocking: whether the function should return only after the trajectory is completed
:param start_joints: optional starting position to move to before running
:return:

<h2 id="iiwa_py.IIWAInterface.delta_pose_trajectory">delta_pose_trajectory</h2>

```python
IIWAInterface.delta_pose_trajectory(self, trajectory, times, blocking=True, start_joints=None)
```

Plays a trajectory of pose commands that are executed at specified times
:param trajectory: Must be iterable providing a 7-length array at each iteration
:param times: an iterable of floats specifying how far from the beginning each point should be executed
:param blocking: whether the function should return only after the trajectory is completed
:param start_joints: optional starting position to move to before running
:return:

<h2 id="iiwa_py.IIWAInterface.joint_vel_trajectory">joint_vel_trajectory</h2>

```python
IIWAInterface.joint_vel_trajectory(self, trajectory, rate=100, blocking=True, start_joints=None)
```

Plays a trajectory of joint velocity commands
:param trajectory: Must be iterable providing a 7-length array at each iteration
:param rate: the rate to publish commands
:param blocking: whether the function should return only after the trajectory is completed
:param start_joints: optional starting position to move to before running
:return:

<h2 id="iiwa_py.IIWAInterface.set_joint_positions">set_joint_positions</h2>

```python
IIWAInterface.set_joint_positions(self, a1=None, a2=None, a3=None, a4=None, a5=None, a6=None, a7=None)
```
Sets the joint positions of specific joints without changing the others
<h2 id="iiwa_py.IIWAInterface.blocking_joint_pos_cmd">blocking_joint_pos_cmd</h2>

```python
IIWAInterface.blocking_joint_pos_cmd(self, positions, timeout=None)
```
Sets the joint positions and blocks until they are reached
<h2 id="iiwa_py.IIWAInterface.set_joint_velocities">set_joint_velocities</h2>

```python
IIWAInterface.set_joint_velocities(self, a1=None, a2=None, a3=None, a4=None, a5=None, a6=None, a7=None)
```
Sets the joint velocities of specific joints without changing the others
<h2 id="iiwa_py.IIWAInterface.increment_pose">increment_pose</h2>

```python
IIWAInterface.increment_pose(self, x=0.0, y=0.0, z=0.0, qx=0.0, qy=0.0, qz=0.0, qw=0.0)
```
Adds to the current pose of specified elements
<h2 id="iiwa_py.IIWAInterface.delta_joint_vel_trajectory">delta_joint_vel_trajectory</h2>

```python
IIWAInterface.delta_joint_vel_trajectory(self, trajectory, times, blocking=True, start_joints=None)
```

Plays a trajectory of joint velocity commands that are executed at specified times
:param trajectory: Must be iterable providing a 7-length array at each iteration
:param times: an iterable of floats specifying how far from the beginning each point should be executed
:param blocking: whether the function should return only after the trajectory is completed
:param start_joints: optional starting position to move to before running
:return:

<h2 id="iiwa_py.IIWAInterface.increment_joint_velocities">increment_joint_velocities</h2>

```python
IIWAInterface.increment_joint_velocities(self, a1=0.0, a2=0.0, a3=0.0, a4=0.0, a5=0.0, a6=0.0, a7=0.0)
```
Adds to the current velocity of specified joints
<h2 id="iiwa_py.IIWAInterface.joint_pos_vel_trajectory">joint_pos_vel_trajectory</h2>

```python
IIWAInterface.joint_pos_vel_trajectory(self, trajectory, rate=100, blocking=True, start_joints=None)
```

Plays a trajectory of joint position-velocity commands
:param trajectory: Must be iterable providing a 14-length array or (7, 7) arrays at each iteration
:param rate: the rate to publish commands
:param blocking: whether the function should return only after the trajectory is completed
:param start_joints: optional starting position to move to before running
:return:

<h2 id="iiwa_py.IIWAInterface.transform_trajectory">transform_trajectory</h2>

```python
IIWAInterface.transform_trajectory(self, trajectory, rate=100, blocking=True, start_joints=None)
```

Plays a trajectory of pose commands
:param trajectory: Must be iterable providing a (4, 4) array at each iteration
:param rate: the rate to publish commands
:param blocking: whether the function should return only after the trajectory is completed
:param start_joints: optional starting position to move to before running
:return:

<h2 id="iiwa_py.IIWAInterface.twist_trajectory">twist_trajectory</h2>

```python
IIWAInterface.twist_trajectory(self, trajectory, rate=100, blocking=True, start_joints=None)
```

Plays a trajectory of twist commands interpolated into pose commands
:param trajectory: Must be iterable providing a 6-length array at each iteration
:param rate: the rate to publish commands
:param blocking: whether the function should return only after the trajectory is completed
:param start_joints: optional starting position to move to before running
:return:

<h2 id="iiwa_py.IIWAInterface.delta_twist_trajectory">delta_twist_trajectory</h2>

```python
IIWAInterface.delta_twist_trajectory(self, trajectory, times, blocking=True, start_joints=None)
```

Plays a trajectory of twist commands interpolated into pose commands that are executed at specified times
:param trajectory: Must be iterable providing a 6-length array at each iteration
:param times: an iterable of floats specifying how far from the beginning each point should be executed
:param blocking: whether the function should return only after the trajectory is completed
:param start_joints: optional starting position to move to before running
:return:

<h2 id="iiwa_py.IIWAInterface.joint_pos_trajectory">joint_pos_trajectory</h2>

```python
IIWAInterface.joint_pos_trajectory(self, trajectory, rate=100, blocking=True, start_joints=None)
```

Plays a trajectory of joint position commands
:param trajectory: Must be iterable providing a 7-length array at each iteration
:param rate: the rate to publish commands
:param blocking: whether the function should return only after the trajectory is completed
:param start_joints: optional starting position to move to before running
:return:

<h2 id="iiwa_py.IIWAInterface.increment_joint_positions">increment_joint_positions</h2>

```python
IIWAInterface.increment_joint_positions(self, a1=0.0, a2=0.0, a3=0.0, a4=0.0, a5=0.0, a6=0.0, a7=0.0)
```
Adds to the current position of specified joints
<h2 id="iiwa_py.IIWAInterface.delta_joint_pos_vel_trajectory">delta_joint_pos_vel_trajectory</h2>

```python
IIWAInterface.delta_joint_pos_vel_trajectory(self, trajectory, times, blocking=True, start_joints=None)
```

Plays a trajectory of joint position-velocity commands that are executed at specified times
:param trajectory: Must be iterable providing a 14-length array or (7, 7) arrays at each iteration
:param times: an iterable of floats specifying how far from the beginning each point should be executed
:param blocking: whether the function should return only after the trajectory is completed
:param start_joints: optional starting position to move to before running
:return:

<h2 id="iiwa_py.IIWAInterface.delta_transform_trajectory">delta_transform_trajectory</h2>

```python
IIWAInterface.delta_transform_trajectory(self, trajectory, times, blocking=True, start_joints=None)
```

Plays a trajectory of pose commands that are executed at specified times
:param trajectory: Must be iterable providing a (4, 4) array at each iteration
:param times: an iterable of floats specifying how far from the beginning each point should be executed
:param blocking: whether the function should return only after the trajectory is completed
:param start_joints: optional starting position to move to before running
:return:

<h2 id="iiwa_py.IIWAInterface.pose_trajectory">pose_trajectory</h2>

```python
IIWAInterface.pose_trajectory(self, trajectory, rate=100, blocking=True, start_joints=None)
```

Plays a trajectory of pose commands
:param trajectory: Must be iterable providing a 7-length array at each iteration
:param rate: the rate to publish commands
:param blocking: whether the function should return only after the trajectory is completed
:param start_joints: optional starting position to move to before running
:return:

