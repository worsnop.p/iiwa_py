#!/usr/bin/env python
import numpy as np
from iiwa_py import startup_iiwa_interface
import rospy
from random import getrandbits
from sound_play.libsoundplay import SoundClient
from std_srvs.srv import SetBool
from copy import deepcopy


class BinaryDecisionState(object):

    def __init__(self, val, next_0=None, next_1=None):
        self.val = val
        self.next_0 = next_0
        self.next_1 = next_1

    def next_state(self, action):
        if action:
            return self.next_1

        else:
            return self.next_0

    def value(self):
        return self.val

    def set_next_states(self, next_0=None, next_1=None):
        if next_0 is not None:
            self.next_0 = next_0

        if next_1 is not None:
            self.next_1 = next_1


class SquareStateMachine(object):

    def __init__(self, x0, x1, x2):
        self.x0 = BinaryDecisionState(x0)
        self.x1 = BinaryDecisionState(x1, self.x0)
        self.x2 = BinaryDecisionState(x2, self.x1)
        self.x3 = BinaryDecisionState(x0 + (x2 - x1), self.x2, self.x0)
        self.x0.set_next_states(self.x3, self.x1)
        self.x1.set_next_states(next_1=self.x2)
        self.x2.set_next_states(next_1=self.x3)

        self.x = self.x0

    def random_step(self):
        return self.step(bool(getrandbits(1)))

    def step(self, action):
        self.x=self.x.next_state(action)
        return self.x.value()

    def current_val(self):
        return self.x.value()


class SimpleSquareGenerator(object):

    def __init__(self, rate, p0, x1, x2):
        self.ssm = SquareStateMachine(p0[:3], x1, x2)
        self.p0 = p0
        self.q = p0[3:]
        self.corners = [self.ssm.current_val()]

    def clear(self):
        self.corners = [self.ssm.current_val()]

    def add_random_edge(self):
        self.corners.append(self.ssm.random_step())

    def add_forward_edge(self):
        self.corners.append(self.ssm.step(True))

    def add_backward_edge(self):
        self.corners.append(self.ssm.step(False))

    def add_edge(self, direction):
        self.corners.append(self.ssm.step(direction))

    def add_steps(self, directions):
        for d in directions:
            self.add_edge(d)

    def add_random_edges(self, N):
        for i in range(N):
            self.add_random_edge()
            

class LinearWayPointPlayer(object):
    
    def __init__(self, interface, pause_cb):
        self.interface = interface
        self.pose_waypoints = None
        self.joint_waypoints = None
        self.pauses = None
        self.start_joints=None
        self.pause_cb = pause_cb
        
    def set_pause_cb(self, pause_cb):
        self.pause_cb = pause_cb

    def clear_pause_cb(self):
        self.pause_cb = None

    def load_start_joints(self, start_joints):
        self.start_joints = deepcopy(start_joints)

    def load_pose_waypoints(self, pose_waypoints):
        self.pose_waypoints = deepcopy(pose_waypoints)

    def load_joint_waypoints(self, joint_waypoints):
        self.joint_waypoints = deepcopy(joint_waypoints)

    def load_pauses(self, pauses):
        self.pauses = deepcopy(pauses)

    def load_identical_pauses(self, pause, match_poses):
        if match_poses:
            self.pauses = [pause]*len(self.pose_waypoints)
        else:
            self.pauses = [pause]*len(self.joint_waypoints)

    def play_pose_waypoints(self, move_to_start=False, ask=False):
        if move_to_start:
            self.interface.pose_cmd(self.pose_waypoints[0], blocking=True)

        if ask:
            raw_input('Ready?')

        for p, w in zip(self.pauses, self.pose_waypoints):
            self.interface.pose_cmd(w, linear=True, blocking=True)
            if self.pause_cb is not None:
                self.pause_cb()
            rospy.sleep(p)

    def play_joint_waypoints(self, move_to_start=False, ask=False):
        if move_to_start:
            self.interface.pos_cmd(self.joint_waypoints[0], blocking=True)

        if ask:
            raw_input('Ready?')

        for p, w in zip(self.pauses, self.joint_waypoints):
            self.interface.pos_cmd(w, blocking=True)
            if self.pause_cb is not None:
                self.pause_cb()
            rospy.sleep(p)

    def copy(self):
        new = LinearWayPointPlayer(self.interface, self.pause_cb)
        new.load_pose_waypoints(self.pose_waypoints)
        new.load_joint_waypoints(self.joint_waypoints)
        new.load_pauses(self.pauses)
        new.load_start_joints(self.start_joints)
        return new


class DynamicWrapper(object):

    def __init__(self, pre, post, pre_args=(), post_args=()):
        self.pre = pre
        self.post = post
        self.pre_args = pre_args
        self.post_args = post_args
        self.func = None

    def load(self, func):
        self.func = func
        return self

    def pre_args(self, *args):
        self.pre_args = args
        return self

    def post_args(self, *args):
        self.post_args = args
        return self

    def call(self, *args, **kwargs):
        self.pre(*self.pre_args)
        self.func(*args, **kwargs)
        self.post(*self.post_args)


def square_points(p0, edge_width, n, n_offset=0.0, flip=None):
    d1 = (n + 1) % 3
    d2 = (n + 2) % 3

    e1 = -edge_width if flip == d1 or flip == n else edge_width

    e2 = -edge_width if flip == d2 or flip == n else edge_width

    p0 = p0.copy()
    p0[n] += n_offset
    x1 = p0.copy()[:3]
    x1[d1] += e1
    x2 = x1.copy()
    x2[d2] += e2
    return p0, x1, x2


sound_client = SoundClient()
k = [1]


def pause_cb():
    print('Ding %s' % k[0])
    sound_client.play(2)
    k[0] += 1


rec = rospy.ServiceProxy('record_bag', SetBool)


def start_rec():
    rec(True)


def stop_rec():
    rec(False)


rec_wrapper = DynamicWrapper(start_rec, stop_rec)


dirs = [False,
        False,
        True,
        False,
        True,
        True,
        True,
        True,
        True,
        False,
        False,
        False,
        True,
        False,
        True,
        False]

interface = startup_iiwa_interface('trajectory_demos')
rospy.set_param('participant_name', raw_input('\nParticipant name:'))
hi_five_edge_width = 0.3
edge_width = 0.2

print('Zero Joints')
rospy.sleep(0.1)
interface.zero_joints()
rospy.sleep(1.0)
print('Crouching')
interface.increment_pose(x=0.3, y=-hi_five_edge_width / 2, z=-0.25)
rospy.sleep(4.0)
print('Done')
p0 = interface.get_pose()
j0 = interface.get_pos()

hi_five_pts = square_points(p0, hi_five_edge_width, n=0, n_offset=edge_width, flip=2)
hi_five_gen = SimpleSquareGenerator(400, *hi_five_pts)
[hi_five_gen.add_forward_edge() for i in range(4)]
hi_five_gen.add_random_edges(4)
[hi_five_gen.add_backward_edge() for i in range(4)]
hi_five_player = LinearWayPointPlayer(interface, pause_cb)
hi_five_player.load_pose_waypoints(hi_five_gen.corners)
hi_five_player.load_identical_pauses(3.0, True)

raw_input('Hi Five')
interface.zero_joints()
interface.pose_cmd(p0)
rec_wrapper.load(hi_five_player.play_pose_waypoints).call(move_to_start=True, ask=True)

z_offsets = -np.arange(0, 0.25, 0.12)
gens = [SimpleSquareGenerator(400, *square_points(p0, edge_width, 2, dz)) for dz in z_offsets]
loud_players = []
quiet_players = []
for gen in gens:
    gen.add_steps(dirs)
    loud_player = LinearWayPointPlayer(interface, pause_cb)
    loud_player.load_pose_waypoints(gen.corners)
    loud_player.load_identical_pauses(3.0, True)
    loud_players.append(loud_player)
    quiet_player = loud_player.copy()
    quiet_player.clear_pause_cb()
    quiet_player.load_identical_pauses(0.01, True)
    quiet_players.append(quiet_player)


print('Zero Joints')
rospy.sleep(0.1)
interface.zero_joints()
rospy.sleep(1.0)
print('Crouching')
interface.increment_pose(x=0.25, y=-hi_five_edge_width / 2, z=-0.25)
rospy.sleep(4.0)
print('Done')

for i, player in enumerate(loud_players):
    raw_input('Loud player %d' % i)
    rec_wrapper.load(player.play_pose_waypoints).call(move_to_start=True, ask=False)

k[0] = 1

for i, player in enumerate(quiet_players):
    raw_input('Quiet player %d' % i)
    rec_wrapper.load(player.play_pose_waypoints(move_to_start=True, ask=False))
