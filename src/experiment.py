#!/usr/bin/env python
import numpy as np
from iiwa_py import startup_iiwa_interface
import rospy
from random import getrandbits
from sound_play.libsoundplay import SoundClient
from scipy.special import expit
from copy import deepcopy


class BinaryDecisionState(object):

    def __init__(self, val, next_0=None, next_1=None):
        self.val = val
        self.next_0 = next_0
        self.next_1 = next_1

    def next_state(self, action):
        if action:
            return self.next_1

        else:
            return self.next_0

    def value(self):
        return self.val

    def set_next_states(self, next_0=None, next_1=None):
        if next_0 is not None:
            self.next_0 = next_0

        if next_1 is not None:
            self.next_1 = next_1


class SquareStateMachine(object):

    def __init__(self, x0, x1, x2):
        self.x0 = BinaryDecisionState(x0)
        self.x1 = BinaryDecisionState(x1, self.x0)
        self.x2 = BinaryDecisionState(x2, self.x1)
        self.x3 = BinaryDecisionState(x0 + (x2 - x1), self.x2, self.x0)
        self.x0.set_next_states(self.x3, self.x1)
        self.x1.set_next_states(next_1=self.x2)
        self.x2.set_next_states(next_1=self.x3)

        self.x = self.x0

    def random_step(self):
        return self.step(bool(getrandbits(1)))

    def step(self, action):
        self.x=self.x.next_state(action)
        return self.x.value()

    def current_val(self):
        return self.x.value()


class PositionalTrajectoryGenerator(object):

    def __init__(self, rate, T):
        self.rate = rate
        self.T = T

    def trajectory(self, x0, x1, q=(0, 0, 0, 1)):
        K = int(self.rate * self.T)
        x0 = np.concatenate((x0, q))
        x1 = np.concatenate((x1, q))
        x01 = x1 - x0
        deltas = expit(np.linspace(-2, 2, K))
        deltas = deltas - deltas[0]
        deltas = deltas / deltas[-1]

        return [x0 + x01 * delta for delta in deltas]


class MultiTrajectoryPlayer(object):

    def __init__(self, interface, rate, pause_cb=None):
        self.interface = interface
        self.rate = rate
        self.pose_trajectories = None
        self.joint_trajectories = None
        self.pauses = None
        self.start_joints=None
        self.pause_cb = pause_cb

    def set_pause_cb(self, pause_cb):
        self.pause_cb = pause_cb

    def clear_pause_cb(self):
        self.pause_cb = None

    def load_start_joints(self, start_joints):
        self.start_joints = deepcopy(start_joints)

    def load_pose_trajectories(self, pose_trajectories):
        self.pose_trajectories = deepcopy(pose_trajectories)

    def load_joint_trajectories(self, joint_trajectories):
        self.joint_trajectories = deepcopy(joint_trajectories)

    def load_pauses(self, pauses):
        self.pauses = deepcopy(pauses)

    def play_pose_trajectories(self, move_to_start=True):
        if move_to_start:
            interface.pose_cmd(self.pose_trajectories[0][0], blocking=True)
        for p, t in zip(self.pauses, self.pose_trajectories):
            if self.pause_cb is not None:
                self.pause_cb()
            rospy.sleep(p)
            self.interface.pose_trajectory(t, self.rate, blocking=True, biased=False)

    def play_joint_trajectories(self, move_to_start=True):
        if move_to_start:
            interface.blocking_joint_pos_cmd(self.joint_trajectories[0][0][:7])
        for p, t in zip(self.pauses, self.joint_trajectories):
            if self.pause_cb is not None:
                self.pause_cb()
            rospy.sleep(p)
            self.interface.joint_pos_vel_trajectory(t, self.rate, blocking=True)

    def convert_pose_trajectories(self):
        self.joint_trajectories = []
        start_joints = self.start_joints
        for t in self.pose_trajectories:
            self.joint_trajectories.append(self.interface.invert_pose_trajectory_smooth(t, self.rate, start_joints,
                                                                                        biased=False))
            start_joints = self.joint_trajectories[-1][-1][:7]

    def copy(self):
        new = MultiTrajectoryPlayer(self.interface, self.rate, self.pause_cb)
        new.load_pose_trajectories(self.pose_trajectories)
        new.load_joint_trajectories(self.joint_trajectories)
        new.load_pauses(self.pauses)
        new.load_start_joints(self.start_joints)
        return new


class SquareGenerator(object):

    def __init__(self, rate, p0, x1, x2, T):
        self.traj_gen = PositionalTrajectoryGenerator(rate, T)
        x1 = self.traj_gen.trajectory(p0[:3], x1)[-1][:3]
        x2 = self.traj_gen.trajectory(x1, x2)[-1][:3]
        self.ssm = SquareStateMachine(p0[:3], x1, x2)
        self.p0 = p0
        self.q = p0[3:]
        self.trajectories = []

    def clear(self):
        self.trajectories = []

    def add_random_edge(self):
        self.trajectories.append(self.traj_gen.trajectory(self.ssm.current_val(), self.ssm.random_step(), self.q))

    def add_forward_edge(self):
        self.trajectories.append(self.traj_gen.trajectory(self.ssm.current_val(), self.ssm.step(True), self.q))

    def add_backward_edge(self):
        self.trajectories.append(self.traj_gen.trajectory(self.ssm.current_val(), self.ssm.step(False), self.q))

    def add_random_edges(self, N):
        for i in range(N):
            self.add_random_edge()


def square_points(p0, edge_width, z_offset=0):
    p0 = p0 + np.array((0, 0, z_offset, 0, 0, 0, 0))
    x1 = p0.copy()[:3]
    x1[0] += edge_width
    x2 = x1.copy()
    x2[1] += edge_width
    return p0, x1, x2


sound_client = SoundClient()
k = [1]


def pause_cb():
    print('Ding %s' % k[0])
    sound_client.play(2)
    k[0] += 1


interface = startup_iiwa_interface('trajectory_demos')

print('Zero Joints')
interface.zero_joints()
rospy.sleep(2.0)
print('Crouching')
edge_width = 0.225
T=0.45

interface.increment_pose(x=0.30, y=-edge_width/2, z=-0.3)
rospy.sleep(4.0)
print('Done')
p0 = interface.get_pose()
j0 = interface.get_pos()

z_offsets = -np.arange(0, 0.21, 0.1)
gens = [SquareGenerator(400, *square_points(p0, edge_width, dz), T=T) for dz in z_offsets]
loud_players = []
quiet_players = []
for gen in gens:
    gen.add_random_edges(16)
    loud_player = MultiTrajectoryPlayer(interface, 100, pause_cb)
    loud_player.load_pose_trajectories(gen.trajectories)
    loud_player.convert_pose_trajectories()
    loud_player.load_pauses([1.0] * len(gens[0].trajectories))
    loud_players.append(loud_player)
    quiet_player = loud_player.copy()
    quiet_player.clear_pause_cb()
    quiet_players.append(quiet_player)

for i, player in enumerate(loud_players):
    print ('Loud player %d' % i)
    player.play_joint_trajectories(move_to_start=True)

k[0] = 1

for i, player in enumerate(quiet_players):
    print ('Quiet player %d' % i)
    player.play_joint_trajectories(move_to_start=True)
