#!/usr/bin/env python
import rospy, rosgraph
import os, signal
from subprocess import Popen
from std_srvs.srv import SetBool


rospy.init_node('bag_recorder')
master = rosgraph.Master('/rostopic')


def has_one(string, substrings):
    for s in substrings:
        if s in string:
            return True

    return False


def rostopic_list(substrings=None):
    if isinstance(substrings, str):
        substrings = [substrings]
    pubs, subs, _ = master.getSystemState()
    topics = list(set(zip(*pubs)[0]) | set(zip(*subs)[0]))

    if substrings is None:
        return substrings

    else:
        return [t for t in topics if has_one(t, substrings)]


class BagRecorder(object):

    def __init__(self, topics):
        self.topics = topics
        self.id = 0
        self.proc = None
        self.recording = False
        self.service = rospy.Service('record_bag', SetBool, self.cb)

    def start(self):
        if not self.recording:
            self.proc = Popen(['rosbag', 'record', '-O', rospy.get_param('participant_name') + '_' +
                               str(self.id)] + self.topics, preexec_fn=os.setsid)
            self.recording = True

    def stop(self):
        if self.recording:
            os.killpg(self.proc.pid, signal.SIGINT)
            self.id += 1
            self.recording = False

    def cb(self, req):
        if req.data:
            self.start()
            return True, 'Started'
        else:
            self.stop()
            return True, 'Stopped'


topic_nss = ['iiwa/command', 'iiwa/state', 'mocap_point_cloud']
desired_topics = rostopic_list(topic_nss)

recorder = BagRecorder(desired_topics)
rospy.spin()