from urdf_parser_py.urdf import URDF
from pykdl_utils.kdl_kinematics import KDLKinematics
from rospkg import RosPack
import xacro


BASE_LINK = 'iiwa_link_0'
END_LINK = 'iiwa_link_ee'


def get_kdl_kin(version=7):
    xacro_path = RosPack().get_path('iiwa_description') + '/urdf/iiwa%d.urdf.xacro' % version
    urdf_str = xacro.process_file(xacro_path, in_order=True, mappings={}).toprettyxml(indent='  ')
    robot = URDF.from_xml_string(urdf_str)
    return KDLKinematics(robot, BASE_LINK, END_LINK)
