from iiwa_msgs.msg import JointPosition, JointVelocity, JointPositionVelocity
from geometry_msgs.msg import PoseStamped, WrenchStamped
from std_msgs.msg import Time
from tf.transformations import quaternion_multiply, quaternion_from_euler
from Queue import Queue
from tools import *
from viz import IIWAVisualizer
from kdl import get_kdl_kin


class StateHolder(object):
    """
    Subscribes to a topic and stores the latest message.
    Has options to mutate this message before memory, append to a buffer and attach a second callback to the topic.
    """

    def __init__(self, topic, msg_type, mutator=None, init=True):
        """
        Constructor
        :param str topic: The topic to subscribe to
        :param msg_type: The type of messages expected on the topic
        :param mutator: a functin which takes a single object of type msg_type and returns the state to store
        :param bool init: True if subscription is to start immediately, False if state holder is to start idle
        """
        self.topic = topic
        self.msg_type = msg_type
        self.mutator = mutator
        self._state = None
        self._sub = None
        self._update = self._update_mutate if self.mutator is not None else self._update_raw
        self._buffer = None
        self._external_cb = None

        if init:
            self.init()

    def init(self):
        """
        Begins subscribing to the topic
        :return: 
        """
        if self._sub is None:
            self._sub = rospy.Subscriber(self.topic, self.msg_type, self._cb)

    def sleep(self):
        """
        Unsubscribes from the topic and puts the state holder in idle mode 
        :return: 
        """
        if self._sub is not None:
            self._sub.unregister()
            self._sub = None

    def active(self):
        """
        Checks if the state holder is currently subscribing
        :return: 
        """
        return self._sub is None

    def register_buffer(self, buf):
        """
        Registers a buffer (must have an append method) to begin recording states
        :param buf: 
        :return: 
        """
        self._buffer = buf

    def clear_buffer(self):
        """
        Removes the current buffer
        :return: 
        """
        self._buffer = None

    def register_callback(self, callback):
        """
        Adds another callback (must take one argument) to be called on each new message
        :param callback: 
        :return: 
        """
        self._external_cb = callback

    def clear_callback(self):
        """
        Clears the current external callback
        :return: 
        """
        self._external_cb = None

    def _update_raw(self, msg):
        """
        Internal state update with no mutation
        :param msg: 
        :return: 
        """
        self._state = msg

    def _update_mutate(self, msg):
        """
        Internal state update with mutation
        :param msg: 
        :return: 
        """
        self._state = self.mutator(msg)

    def _cb(self, msg):
        """
        Internal callback to be called with each new message
        :param msg: 
        :return: 
        """
        self._update(msg)
        if self._buffer:
            self._buffer.append(self._state)

        if self._external_cb:
            self._external_cb(self._state)

    def state(self):
        """
        returns the current state
        :return: 
        """
        return self._state

    def history(self):
        """
        Returns the current buffer
        :return: 
        """
        return self._buffer

    def pop_history(self):
        """
        Returns the current buffer and removes it
        :return: 
        """
        hist = self._buffer
        self._buffer = None
        return hist


class IIWASubscriber(object):
    """
    Sets up state holders for all the relevant topics of the iiwa and methods to interface with them.
    """

    def __init__(self, as_arrays=False, read_pos=True, read_vel=True, read_torque=True, read_pose=True,
                 read_wrench=True, read_stiffness=True, read_damping=True, read_destination=True):
        """
        Constructor
        :param bool as_arrays: Determines if states are converted to arrays or kept as ROS messages
        :param bool read_pos: True if this state is to be tracked
        :param bool read_vel: True if this state is to be tracked
        :param bool read_torque: True if this state is to be tracked
        :param bool read_pose: True if this state is to be tracked
        :param bool read_wrench: True if this state is to be tracked
        :param bool read_stiffness: True if this state is to be tracked
        :param bool read_damping: True if this state is to be tracked
        :param bool read_destination: True if this state is to be tracked
        """
        super(IIWASubscriber, self).__init__()
        self.pos_state = StateHolder('/iiwa/state/JointPosition', JointPosition, joint_pos_array if as_arrays else None,
                                     read_pos)
        self.vel_state = StateHolder('/iiwa/state/JointVelocity', JointVelocity, joint_vel_array if as_arrays else None,
                                     read_vel)
        self.torque_state = StateHolder('/iiwa/state/JointTorque', JointTorque,
                                        joint_torque_array if as_arrays else None, read_torque)
        self.pose_state = StateHolder('/iiwa/state/CartesianPose', PoseStamped, pose_array if as_arrays else None,
                                      read_pose)
        self.wrench_state = StateHolder('/iiwa/state/CartesianWrench', WrenchStamped, wrench_array if as_arrays else None,
                                        read_wrench)
        self.stiff_state = StateHolder('/iiwa/state/JointStiffness', JointStiffness,
                                       joint_stiffness_array if as_arrays else None, read_stiffness)
        self.damp_state = StateHolder('/iiwa/state/JointDamping', JointDamping,
                                      joint_damping_array if as_arrays else None, read_damping)
        self.dest_state = StateHolder('/iiwa/state/DestinationReached', Time, time_float if as_arrays else None,
                                      read_destination)
        self.state_dict = {'position': self.pos_state,
                           'velocity': self.vel_state,
                           'torque': self.torque_state,
                           }
        self._states = {'position': self.pos_state, 'velocity': self.vel_state, 'torque': self.torque_state,
                        'pose': self.pos_state, 'wrench': self.wrench_state, 'stiffness': self.stiff_state,
                        'damping': self.damp_state, 'destination': self.dest_state}
        self._buffers = {}

    def get_pos(self):
        """Returns the current joint positions"""
        return self.pos_state.state()

    def get_vel(self):
        """Returns the current joint velocities"""
        return self.vel_state.state()

    def get_torque(self):
        """Returns the current joint torques"""
        return self.torque_state.state()

    def get_pose(self):
        """Returns the current end-effector pose"""
        return self.pose_state.state()

    def get_wrench(self):
        """Returns the current end-effector wrench"""
        return self.wrench_state.state()

    def get_stiffness(self):
        """Returns the current joint stiffness"""
        return self.stiff_state.state()

    def get_damping(self):
        """Returns the current joint damping"""
        return self.damp_state.state()

    def get_time_of_completion(self):
        """Returns the last time a command was completed"""
        return self.dest_state.state()

    def record_states(self, *states):
        """
        Starts recording the states passed
        :param states: each argument in states must be one of ['position', 'velocity', 'torque', 'pose', 'wrench',
        'stiffness', 'damping', 'destination']
        :return:
        """
        for state in states:
            self._buffers[state] = []
            self._states[state].register_buffer(self._buffers[state])

    def stop_recording_states(self, *states):
        """
        Stops recording the states passed and returns their histories
        :param states:
        :return: A dictionary of the state histories
        """
        return_buffers = {}
        for state in states:
            self._states[state].clear_buffer()
            return_buffers[state] = self._buffers.pop(state)

        return return_buffers


class IIWACommander(object):
    """
    Sets up publishers to all the command topics of the iiwa and provides methods to interface with them
    """

    def __init__(self, lin_pose_mode=True):
        super(IIWACommander, self).__init__()
        self.pos_pub = rospy.Publisher('/iiwa/command/JointPosition', JointPosition, queue_size=100)
        self.vel_pub = rospy.Publisher('/iiwa/command/JointVelocity', JointVelocity, queue_size=100)
        self.pose_pub = rospy.Publisher('/iiwa/command/CartesianPose', PoseStamped, queue_size=100)
        self.pose_pub_lin = rospy.Publisher('/iiwa/command/CartesianPoseLin', PoseStamped, queue_size=100)
        self.pos_vel_pub = rospy.Publisher('/iiwa/command/JointPositionVelocity', JointPositionVelocity, queue_size=100)

    def send_pos_msg(self, msg):
        """
        Sends a joint position command message
        :param JointPosition msg: the command message
        :return:
        """
        self.pos_pub.publish(msg)

    def send_vel_msg(self, msg):
        """
        Sends a joint velocity command message
        :param JointVelocity msg: the command message
        :return:
        """
        self.vel_pub.publish(msg)

    def send_pose_msg(self, msg, linear=False):
        """
        Sends a cartesian pose command message
        :param PoseStamped msg: the command message
        :param bool linear: whether to move lienarly to this position or not
        :return:
        """
        self.pose_pub_lin.publish(msg) if linear else self.pose_pub.publish(msg)

    def send_pos_vel_msg(self, msg):
        """
        Sends a postion-velocity command message
        :param JointPositionVelocity msg: the command message
        :return:
        """
        self.pos_vel_pub.publish(msg)

    def set_joint_positions(self, a1=0.0, a2=0.0, a3=0.0, a4=0.0, a5=0.0, a6=0.0, a7=0.0, **kwargs):
        """Sets the joint positions defaulting unspecified joints to zero"""
        self.send_pos_msg(joint_pos_msg(a1, a2, a3, a4, a5, a6, a7))

    def set_joint_velocities(self, a1=0.0, a2=0.0, a3=0.0, a4=0.0, a5=0.0, a6=0.0, a7=0.0, **kwargs):
        """Sets the joint velocities defaulting unspecified joints to zero"""
        self.send_vel_msg(joint_vel_msg(a1, a2, a3, a4, a5, a6, a7))

    def set_pose(self, x=0.0, y=0.0, z=0.0, qx=0.0, qy=0.0, qz=0.0, qw=0.0, linear=False, **kwargs):
        """Sets the cartesian pose defaulting unspecified components to zero"""
        self.send_pose_msg(pose_msg((x, y, z, qx, qy, qz, qw), 'kuka'), linear=linear)

    def pos_cmd(self, positions, blocking=False):
        """Commands the joint positions with an array"""
        self.set_joint_positions(*positions, blocking=blocking)

    def vel_cmd(self, velocities, blocking=False):
        """Commands the joint velocities with an array"""
        self.set_joint_velocities(*velocities, blocking=blocking)

    def pose_cmd(self, pose, linear=False, blocking=False):
        """Commands the cartesian pose with an array"""
        self.set_pose(*pose, linear=linear, blocking=blocking)

    def transform_cmd(self, transform, linear=False, blocking=False):
        """Commands the cartesian pose with transformation matrix"""
        self.set_pose(*transform_to_pose_array(transform), linear=linear, blocking=blocking)

    def pos_vel_cmd(self, pos_vel):
        """
        Commands the joint positions and end velocity with either an array
        :param pos_vel: can be either [position, velocity] (2, 7) or [position]+[velocity] (14,)
        :return:
        """
        pos_vel = np.array(pos_vel).flatten()
        self.send_pos_vel_msg(joint_pos_vel_msg(pos_vel[:7], pos_vel[7:]))


class IIWAInterface(IIWASubscriber, IIWACommander):
    """
    Inherits boht the IIIWASubscriber and IIWACommander and provides additional methods which takes advantage of both.
    """

    def __init__(self):
        super(IIWAInterface, self).__init__(as_arrays=True)
        self.kdl_kin = get_kdl_kin()
        self._visualizer = IIWAVisualizer()
        self._load_kdl_kin_methods()
        self._trajectory = None
        self._trajectory_cmd = None
        self._timer = None

    def simulate_cmds(self):
        self._visualizer.simulate(self.forward_kinematics, self.inverse_kinematics)
        rospy.sleep(0.5)
        self.zero_joints()
        print('SIM')

    def real_cmds(self):
        self._visualizer.stop_simulation()

    def _load_kdl_kin_methods(self):
        self.base_link = self.kdl_kin.base_link
        self.cart_inertia = self.kdl_kin.cart_inertia
        self.chain = self.kdl_kin.chain
        self.joint_limits_lower = self.kdl_kin.joint_limits_lower
        self.clip_joints_safe = self.kdl_kin.clip_joints_safe
        self.joint_limits_upper = self.kdl_kin.joint_limits_upper
        self.difference_joints = self.kdl_kin.difference_joints
        self.joint_safety_lower = self.kdl_kin.joint_safety_lower
        self.end_link = self.kdl_kin.end_link
        self.joint_safety_upper = self.kdl_kin.joint_safety_upper
        self.extract_joint_state = self.kdl_kin.extract_joint_state
        self.joint_types = self.kdl_kin.joint_types
        self.joints_in_limits = self.kdl_kin.joints_in_limits
        self.get_joint_limits = self.kdl_kin.get_joint_limits
        self.joints_in_safe_limits = self.kdl_kin.joints_in_safe_limits
        self.get_joint_names = self.kdl_kin.get_joint_names
        self.num_joints = self.kdl_kin.num_joints
        self.get_link_names = self.kdl_kin.get_link_names
        self.random_joint_angles = self.kdl_kin.random_joint_angles
        self.inertia = self.kdl_kin.inertia
        self.tree = self.kdl_kin.tree
        self.urdf = self.kdl_kin.urdf

    def zero_joints(self, blocking=True):
        """Sets all joint positions to zero"""
        if blocking:
            self.blocking_joint_pos_cmd([0]*7)
        else:
            self.pos_cmd([0]*7)

    def blocking_queue(self):
        """Sets the joint positions and blocks until they are reached"""
        queue = Queue()
        
        def put(finish_time):
            queue.put(finish_time)
        
        self.dest_state.register_callback(put)
        return queue

    def blocking_joint_pos_cmd(self, positions, timeout=None):
        """Sets the joint positions and blocks until they are reached"""
        queue = Queue()

        def put(finish_time):
            queue.put(finish_time)

        self.dest_state.register_callback(put)
        self.pos_cmd(positions)
        return queue.get(block=True, timeout=timeout)

    def set_joint_positions(self, a1=None, a2=None, a3=None, a4=None, a5=None, a6=None, a7=None, blocking=False):
        """Sets the joint positions of specific joints without changing the others"""
        current = self.get_pos()
        a1 = current[0] if a1 is None else a1
        a2 = current[1] if a2 is None else a2
        a3 = current[2] if a3 is None else a3
        a4 = current[3] if a4 is None else a4
        a5 = current[4] if a5 is None else a5
        a6 = current[5] if a6 is None else a6
        a7 = current[6] if a7 is None else a7
        if blocking:
            queue = self.blocking_queue()
            super(IIWAInterface, self).set_joint_positions(a1, a2, a3, a4, a5, a6, a7)
            queue.get(blocking, timeout=20.0)

        else:
            super(IIWAInterface, self).set_joint_positions(a1, a2, a3, a4, a5, a6, a7)

    def set_joint_velocities(self, a1=None, a2=None, a3=None, a4=None, a5=None, a6=None, a7=None, blocking=False):
        """Sets the joint velocities of specific joints without changing the others"""
        current = self.get_vel()
        a1 = current[0] if a1 is None else a1
        a2 = current[1] if a2 is None else a2
        a3 = current[2] if a3 is None else a3
        a4 = current[3] if a4 is None else a4
        a5 = current[4] if a5 is None else a5
        a6 = current[5] if a6 is None else a6
        a7 = current[6] if a7 is None else a7
        if blocking:
            queue = self.blocking_queue()
            super(IIWAInterface, self).set_joint_velocities(a1, a2, a3, a4, a5, a6, a7)
            queue.get(blocking, timeout=20.0)

        else:
            super(IIWAInterface, self).set_joint_velocities(a1, a2, a3, a4, a5, a6, a7)

    def set_pose(self, x=None, y=None, z=None, qx=None, qy=None, qz=None, qw=None, linear=False, blocking=False):
        """Sets the cartesian pose of specific components without changing the others"""
        current = self.get_pose()
        x = current[0] if x is None else x
        y = current[1] if y is None else y
        z = current[2] if z is None else z
        qx = current[3] if qx is None else qx
        qy = current[4] if qy is None else qy
        qz = current[5] if qz is None else qz
        qw = current[6] if qw is None else qw
        if blocking:
            queue = self.blocking_queue()
            super(IIWAInterface, self).set_pose(x, y, z, qx, qy, qz, qw, linear=linear)
            queue.get(blocking, timeout=20.0)

        else:
            super(IIWAInterface, self).set_pose(x, y, z, qx, qy, qz, qw, linear=linear)

    def increment_joint_positions(self, a1=0.0, a2=0.0, a3=0.0, a4=0.0, a5=0.0, a6=0.0, a7=0.0, blocking=False):
        """Adds to the current position of specified joints"""
        self.pos_cmd(self.get_pos()+np.array((a1, a2, a3, a4, a5, a6, a7)), blocking=blocking)

    def increment_joint_velocities(self, a1=0.0, a2=0.0, a3=0.0, a4=0.0, a5=0.0, a6=0.0, a7=0.0, blocking=False):
        """Adds to the current velocity of specified joints"""
        self.vel_cmd(self.get_vel()+np.array((a1, a2, a3, a4, a5, a6, a7)), blocking=blocking)

    def increment_pose(self, x=0.0, y=0.0, z=0.0, qx=0.0, qy=0.0, qz=0.0, qw=1.0, linear=False, blocking=False):
        """Adds to the current pose of specified elements"""
        pose = self.get_pose()
        pose[:3] += np.array((x, y, z))
        pose[3:] = quaternion_multiply(pose[3:], (qx, qy, qz, qw))
        self.pose_cmd(pose, linear=linear, blocking=blocking)

    def rotate(self, x=0.0, y=0.0, z=0.0, axes='sxyz', blocking=False):
        """Rotates the end-effector using euler angles in the specified axes convention"""
        qx, qy, qz, qw = quaternion_from_euler(x, y, z, axes)
        self.increment_pose(qx=qx, qy=qy, qz=qz, qw=qw, blocking=blocking)

    def get_tool_transform(self):
        """Returns the homogenous transformation matrix for the tool frame"""
        pose = self.get_pose()
        return pose_to_transform_array(pose)

    def move_in_tool_frame(self, x=0.0, y=0.0, z=0.0, linear=False, blocking=False):
        """Moves the end effector using increments specified in the tool frame"""
        pose = self.get_pose()
        quat = pose[3:]
        tool_cmd = (x, y, z, 1)
        world_cmd = quaternion_matrix(quat).dot(tool_cmd)[:3]
        pose[:3] += world_cmd
        self.pose_cmd(pose, linear=linear, blocking=blocking)

    def joint_pos_trajectory(self, trajectory, rate=100, blocking=True, start_joints=None, smooth=True):
        """
        Plays a trajectory of joint position commands
        :param trajectory: Must be iterable providing a 7-length array at each iteration
        :param rate: the rate to publish commands
        :param blocking: whether the function should return only after the trajectory is completed
        :param start_joints: optional starting position to move to before running
        :return:
        """
        if smooth:
            joint_velocities = self.differentiate_joint_positions(trajectory, rate)
            self._trajectory = np.hstack((trajectory, joint_velocities))
            self._trajectory_cmd = self.pos_vel_cmd
            self._run_trajectory(rate, blocking, start_joints)

        else:
            self._trajectory = list(trajectory)
            self._trajectory_cmd = self.pos_cmd
            self._run_trajectory(rate, blocking, start_joints)

    def joint_vel_trajectory(self, trajectory, rate=100, blocking=True, start_joints=None):
        """
        Plays a trajectory of joint velocity commands
        :param trajectory: Must be iterable providing a 7-length array at each iteration
        :param rate: the rate to publish commands
        :param blocking: whether the function should return only after the trajectory is completed
        :param start_joints: optional starting position to move to before running
        :return:
        """
        self._trajectory = list(trajectory)
        self._trajectory_cmd = self.vel_cmd
        self._run_trajectory(rate, blocking, start_joints)

    def pose_trajectory(self, trajectory, rate=100, blocking=True, start_joints=None, smooth=True, biased=True):
        """
        Plays a trajectory of pose commands
        :param trajectory: Must be iterable providing a 7-length array at each iteration
        :param rate: the rate to publish commands
        :param blocking: whether the function should return only after the trajectory is completed
        :param start_joints: optional starting position to move to before running
        :return:
        """
        if smooth:
            joint_trajectory = self.invert_pose_trajectory(trajectory, start_joints, biased=biased)
            joint_velocities = self.differentiate_joint_positions(joint_trajectory, rate)
            self._trajectory = list(np.hstack((joint_trajectory, joint_velocities)))
            self._trajectory_cmd = self.pos_vel_cmd
            self._run_trajectory(rate, blocking, start_joints)

        else:
            self._trajectory = list(trajectory)
            self._trajectory_cmd = self.pose_cmd
            self._run_trajectory(rate, blocking, start_joints)

    def joint_pos_vel_trajectory(self, trajectory, rate=100, blocking=True, start_joints=None):
        """
        Plays a trajectory of joint position-velocity commands
        :param trajectory: Must be iterable providing a 14-length array or (7, 7) arrays at each iteration
        :param rate: the rate to publish commands
        :param blocking: whether the function should return only after the trajectory is completed
        :param start_joints: optional starting position to move to before running
        :return:
        """
        self._trajectory = trajectory
        self._trajectory_cmd = self.pos_vel_cmd
        self._run_trajectory(rate, blocking, start_joints)

    def transform_trajectory(self, trajectory, rate=100, blocking=True, start_joints=None):
        """
        Plays a trajectory of pose commands
        :param trajectory: Must be iterable providing a (4, 4) array at each iteration
        :param rate: the rate to publish commands
        :param blocking: whether the function should return only after the trajectory is completed
        :param start_joints: optional starting position to move to before running
        :return:
        """
        self._trajectory = list(map(transform_to_pose_array, trajectory))
        self._trajectory_cmd = self.pose_cmd
        self._run_trajectory(rate, blocking, start_joints)

    def twist_trajectory(self, trajectory, rate=100, blocking=True, start_joints=None):
        """
        Plays a trajectory of twist commands interpolated into pose commands
        :param trajectory: Must be iterable providing a 6-length array at each iteration
        :param rate: the rate to publish commands
        :param blocking: whether the function should return only after the trajectory is completed
        :param start_joints: optional starting position to move to before running
        :return:
        """
        trajectory = np.array(trajectory)
        dt = 1.0/rate
        displacements = trajectory[:, :3] * dt
        rotations = trajectory[:, 3:] * dt
        pose = self.get_pose()
        positions = pose[:3] + np.sum(displacements, axis=0)
        orientations = [pose[3:]]
        for rotation in rotations:
            orientations.append(quaternion_multiply(orientations[-1], quaternion_from_euler(*rotation)))
        orientations.pop(0)
        self._trajectory = list(np.hstack((positions, orientations)))
        self._trajectory_cmd = self.pose_cmd
        self._run_trajectory(rate, blocking, start_joints)

    def delta_joint_pos_trajectory(self, trajectory, times, blocking=True, start_joints=None):
        """
        Plays a trajectory of joint position commands that are executed at specified times
        :param trajectory: Must be iterable providing a 7-length array at each iteration
        :param times: an iterable of floats specifying how far from the beginning each point should be executed
        :param blocking: whether the function should return only after the trajectory is completed
        :param start_joints: optional starting position to move to before running
        :return:
        """
        self._trajectory = list(trajectory)
        self._trajectory_cmd = self.pos_cmd
        self._run_delta_trajectory(times, blocking, start_joints)

    def delta_joint_vel_trajectory(self, trajectory, times, blocking=True, start_joints=None):
        """
        Plays a trajectory of joint velocity commands that are executed at specified times
        :param trajectory: Must be iterable providing a 7-length array at each iteration
        :param times: an iterable of floats specifying how far from the beginning each point should be executed
        :param blocking: whether the function should return only after the trajectory is completed
        :param start_joints: optional starting position to move to before running
        :return:
        """
        self._trajectory = list(trajectory)
        self._trajectory_cmd = self.vel_cmd
        self._run_delta_trajectory(times, blocking, start_joints)

    def delta_pose_trajectory(self, trajectory, times, blocking=True, start_joints=None):
        """
        Plays a trajectory of pose commands that are executed at specified times
        :param trajectory: Must be iterable providing a 7-length array at each iteration
        :param times: an iterable of floats specifying how far from the beginning each point should be executed
        :param blocking: whether the function should return only after the trajectory is completed
        :param start_joints: optional starting position to move to before running
        :return:
        """
        self._trajectory = list(trajectory)
        self._trajectory_cmd = self.pose_cmd
        self._run_delta_trajectory(times, blocking, start_joints)

    def delta_joint_pos_vel_trajectory(self, trajectory, times, blocking=True, start_joints=None):
        """
        Plays a trajectory of joint position-velocity commands that are executed at specified times
        :param trajectory: Must be iterable providing a 14-length array or (7, 7) arrays at each iteration
        :param times: an iterable of floats specifying how far from the beginning each point should be executed
        :param blocking: whether the function should return only after the trajectory is completed
        :param start_joints: optional starting position to move to before running
        :return:
        """
        self._trajectory = trajectory
        self._trajectory_cmd = self.pos_vel_cmd
        self._run_delta_trajectory(times, blocking, start_joints)

    def delta_transform_trajectory(self, trajectory, times, blocking=True, start_joints=None):
        """
        Plays a trajectory of pose commands that are executed at specified times
        :param trajectory: Must be iterable providing a (4, 4) array at each iteration
        :param times: an iterable of floats specifying how far from the beginning each point should be executed
        :param blocking: whether the function should return only after the trajectory is completed
        :param start_joints: optional starting position to move to before running
        :return:
        """
        self._trajectory = list(map(transform_to_pose_array, trajectory))
        self._trajectory_cmd = self.pose_cmd
        self._run_delta_trajectory(times, blocking, start_joints)

    def delta_twist_trajectory(self, trajectory, times, blocking=True, start_joints=None):
        """
        Plays a trajectory of twist commands interpolated into pose commands that are executed at specified times
        :param trajectory: Must be iterable providing a 6-length array at each iteration
        :param times: an iterable of floats specifying how far from the beginning each point should be executed
        :param blocking: whether the function should return only after the trajectory is completed
        :param start_joints: optional starting position to move to before running
        :return:
        """
        trajectory = np.array(trajectory)
        dts = np.append(np.diff(np.array(times)), 0).reshape((-1, 1))
        displacements = trajectory[:, :3] * dts
        rotations = trajectory[:, 3:] * dts
        pose = self.get_pose()
        positions = pose[:3] + np.sum(displacements, axis=0)
        orientations = [pose[3:]]
        for rotation in rotations:
            orientations.append(quaternion_multiply(orientations[-1], quaternion_from_euler(*rotation)))
        orientations.pop(0)
        self._trajectory = list(np.hstack((positions, orientations)))
        self._trajectory_cmd = self.pose_cmd
        self._run_delta_trajectory(times, blocking, start_joints)

    @staticmethod
    def differentiate_joint_positions(trajectory, rate):
        dt = 1.0 / rate
        return np.vstack((np.diff(trajectory, axis=0), np.zeros_like(trajectory[0]))) / dt

    def invert_pose_trajectory(self, trajectory, start_joints=None, biased=True):
        q_last = self.get_pos() if start_joints is None else start_joints
        q_weights = np.ones_like(trajectory[0])
        joint_trajectory = []
        for i, p in enumerate(trajectory):
            if biased:
                joint_trajectory.append(self.inverse_kinematics(p, biased=True, q_init=q_last, q_bias=q_last,
                                                                q_bias_weights=q_weights))
            else:
                joint_trajectory.append(self.inverse_kinematics(p, q_guess=q_last))
            q_last = joint_trajectory[-1].copy()
        return joint_trajectory

    def invert_pose_trajectory_smooth(self, trajectory, rate, start_joints=None, biased=True):
        joint_trajectory = self.invert_pose_trajectory(trajectory, start_joints, biased=biased)
        joint_velocities = self.differentiate_joint_positions(joint_trajectory, rate)
        return list(np.hstack((joint_trajectory, joint_velocities)))

    def _run_trajectory(self, rate, blocking, start_joints):
        """
        Runs the currently saved trajectory
        :param rate: the rate to publish commands
        :param blocking: whether the function should return only after the trajectory is completed
        :param start_joints: optional starting position to move to before running
        :return:
        """
        if start_joints is not None:
            self.blocking_joint_pos_cmd(start_joints)

        self._timer = rospy.Timer(rospy.Duration(1.0/rate), self._trajectory_cb)

        if blocking:
            self._timer.join()
            rospy.sleep(1.0/rate)

    def _run_delta_trajectory(self, times, blocking, start_joints):
        """
        Runs the currently saved trajectory at the given times
        :param times: an iterable of floats specifying how far from the beginning each point should be executed
        :param blocking: whether the function should return only after the trajectory is completed
        :param start_joints: optional starting position to move to before running
        :return:
        """
        if start_joints:
            self.blocking_joint_pos_cmd(start_joints)

        timers = [rospy.Timer(rospy.Duration(time), self._trajectory_cb, oneshot=True) for time in times]
        if blocking:
            timers[-1].join()

    def _trajectory_cb(self, event):
        """
        Callback for running the currently saved trajectory in a separate thread
        """
        try:
            self._trajectory_cmd(self._trajectory.pop(0))
        except IndexError:
            self._timer.shutdown()
            self._timer = None
            self._trajectory = None
            self._trajectory_cmd = None

    def jacobian(self, joints=None):
        joints = self.get_pos() if joints is None else joints
        return self.kdl_kin.jacobian(joints)

    def forward_kinematics(self, joints, as_transform=False):
        return self.kdl_kin.forward(joints) if as_transform else transform_to_pose_array(self.kdl_kin.forward(joints))

    def inverse_kinematics(self, pose, biased=False, search=False, **kwargs):
        transform = pose_to_transform_array(pose)
        return self.inverse_kinematics_from_transform(transform, biased, search, **kwargs)

    def inverse_kinematics_from_transform(self, transform, biased=False, search=False, **kwargs):
        if not biased and not search:
            return self.kdl_kin.inverse(transform, **kwargs)

        if biased and not search:
            return self.kdl_kin.inverse_biased(transform, **kwargs)

        if not biased and search:
            return self.kdl_kin.inverse_search(transform, **kwargs)

        if biased and search:
            return self.kdl_kin.inverse_biased_search(transform, **kwargs)


def startup_iiwa_interface(node_name='iiwa_interface'):
    rospy.init_node(node_name)
    return IIWAInterface()