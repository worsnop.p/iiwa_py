from iiwa_msgs.msg import JointPosition, JointVelocity, JointQuantity, JointTorque, JointStiffness, JointDamping, \
    JointPositionVelocity
from geometry_msgs.msg import Pose, PoseStamped, Point, Quaternion
from std_msgs.msg import Header
from sensor_msgs.msg import JointState
from tf.transformations import quaternion_from_matrix, translation_from_matrix, quaternion_matrix
import rospy
import numpy as np


def header(frame_id=''):
    return Header(stamp=rospy.Time.now(), frame_id=frame_id)


mode_dict = {'position': JointPosition,
             'velocity': JointVelocity,
             'torque': JointTorque,
             'stiffness': JointStiffness,
             'damping': JointDamping}


def unit_quaternion():
    return np.array((0, 0, 0, 1))


def unit_quaternion_msg():
    return Quaternion(w=1.0)


def joint_msg(mode, a1=0.0, a2=0.0, a3=0.0, a4=0.0, a5=0.0, a6=0.0, a7=0.0):
    kwargs = {'header': header(), mode: JointQuantity(a1=a1, a2=a2, a3=a3, a4=a4, a5=a5, a6=a6, a7=a7)}
    return mode_dict[mode](**kwargs)


def joint_pos_msg(a1=0.0, a2=0.0, a3=0.0, a4=0.0, a5=0.0, a6=0.0, a7=0.0):
    return joint_msg('position', a1, a2, a3, a4, a5, a6, a7)


def joint_vel_msg(a1=0.0, a2=0.0, a3=0.0, a4=0.0, a5=0.0, a6=0.0, a7=0.0):
    return joint_msg('velocity', a1, a2, a3, a4, a5, a6, a7)


def joint_torque_msg(a1=0.0, a2=0.0, a3=0.0, a4=0.0, a5=0.0, a6=0.0, a7=0.0):
    return joint_msg('torque', a1, a2, a3, a4, a5, a6, a7)


def joint_pos_vel_msg(position, velocity):
    return JointPositionVelocity(header=header(), position=JointQuantity(*position), velocity=JointQuantity(*velocity))


def pose_msg(pose, frame_id='', stamped=True):
    if stamped:
        return PoseStamped(header=header(frame_id),
                       pose=Pose(position=Point(*pose[:3]), orientation=Quaternion(*pose[3:])))
    else:
        return Pose(position=Point(*pose[:3]), orientation=Quaternion(*pose[3:]))


def transform_to_pose_array(transform):
    return np.concatenate((translation_from_matrix(transform), quaternion_from_matrix(transform)))


def pose_to_transform_array(pose):
    r, q = pose[:3], pose[3:]
    transform = quaternion_matrix(q)
    transform[:3, 3] = r
    return transform


def transform_to_pose_msg(transform, frame_id=''):
    return pose_msg(np.concatenate((translation_from_matrix(transform), quaternion_from_matrix(transform))), frame_id)


def joint_array(mode, msg):
    jq = getattr(msg, mode)
    return np.array((jq.a1, jq.a2, jq.a3, jq.a4, jq.a5, jq.a6, jq.a7), dtype=float)


def get_joint_array_func(mode):
    def func(msg):
        return joint_array(mode, msg)
    return func


def joint_pos_array(msg):
    return joint_array('position', msg)


def joint_vel_array(msg):
    return joint_array('velocity', msg)


def joint_torque_array(msg):
    return joint_array('torque', msg)


def joint_stiffness_array(msg):
    return joint_array('stiffness', msg)


def joint_damping_array(msg):
    return joint_array('damping', msg)

def pos_vel_msg_to_joint_state(msg, names):
    """

    :param JointPositionVelocity msg:
    :param names:
    :return:
    """
    return JointState(name=names, position=joint_array('position', msg), velocity=joint_array('velocity', msg))

def vector3_array(msg):
    return np.array((msg.x, msg.y, msg.z))


def quaternion_array(msg):
    return np.array((msg.x, msg.y, msg.z, msg.w))


def pose_array(msg):
    return np.concatenate((vector3_array(msg.pose.position), quaternion_array(msg.pose.orientation)))


def wrench_array(msg):
    return np.concatenate((vector3_array(msg.wrench.force), vector3_array(msg.wrench.torque)))


def time_float(msg):
    return msg.data.to_sec()