from nav_msgs.msg import Path, Odometry
from sensor_msgs.msg import JointState
from geometry_msgs.msg import PoseStamped, Twist, Vector3
from std_msgs.msg import Time
from iiwa_msgs.msg import JointPosition, JointVelocity, JointPositionVelocity
import rospy
from tf.transformations import quaternion_multiply, quaternion_inverse
from tools import header, pose_msg, joint_pos_array, joint_vel_array, pos_vel_msg_to_joint_state, joint_pos_msg, \
    joint_vel_msg, transform_to_pose_msg, pose_array
import numpy as np
from std_msgs.msg import Header


def joint_trajectory_to_joint_state_msgs(trajectory, names):
    return [JointState(position=positions, name=names) for positions in trajectory]


def poses_to_odom_msgs(trajectory, dt, child_frame_id='end_effector'):
    twists = np.append(differentiate_pose_trajectory(trajectory, dt), np.zeros(6))
    msgs = []
    for pose, twist in zip(trajectory, twists):
        msg = Odometry()
        msg.child_frame_id = child_frame_id
        msg.pose.pose = pose_msg(pose, stamped=False)
        msg.twist.twist = Twist(linear=Vector3(*twist[:3]), angular=Vector3(*twist[3:]))
        msgs.append(msg)

    return msgs


def odom_msgs_to_path_msg(odom_msgs, frame_id='world'):
    poses = [PoseStamped(header=Header(frame_id=frame_id), pose=msg.pose.pose) for msg in odom_msgs]
    return Path(poses=poses)


def differentiate_pose_trajectory(poses, dt):
    poses = np.asarray(poses)
    points, quats = poses[:, :3], poses[:, 3:]
    lin_vels = np.diff(points, axis=0)
    rot_vels = reduce(diff_quaternions, quats)
    return np.hstack((lin_vels, rot_vels)) / dt


def diff_quaternions(q0, q1):
    relative_quat = quaternion_multiply(quaternion_inverse(q0), q1)
    return quaternion_to_axis_angle(relative_quat)


def quaternion_to_axis_angle(quaternion):
    qx, qy, qz, qw = quaternion
    if qw == 1:
        return np.zeros(3)
    angle = 2 * np.arccos(qw)
    s = np.sqrt(1 - qw * qw)
    x = qx / s
    y = qy / s
    z = qz / s
    return np.array((x, y, z)) * angle


def rotation_matrix_to_axis_angle(rotation_matrix):
    angle = np.arccos((np.sum(np.diag(rotation_matrix)) - 1) / 2)
    x = (rotation_matrix[2, 1] - rotation_matrix[1, 2])
    y = (rotation_matrix[0, 2] - rotation_matrix[2, 0])
    z = (rotation_matrix[1, 0] - rotation_matrix[0, 1])
    axis = np.array((x, y, z))
    return  axis / np.linalg.norm(axis) * angle


class IIWAVisualizer(object):
    name = ['iiwa_joint_%d' % (i + 1) for i in range(7)]

    def __init__(self):
        self.joint_pub = rospy.Publisher('iiwa/sim/joint_states', JointState, queue_size=100)
        self.path_pub = rospy.Publisher('iiwa/sim/path', Path, queue_size=100)
        self.odom_pub = rospy.Publisher('iiwa/sim/odom', Odometry, queue_size=100)
        self._timer = None
        self._trajectory = None
        self._trajectory_cmd = None

        self._current_pos = np.zeros(7)
        self._current_vel = np.zeros(7)

        self._pos_sub = None
        self._vel_sub = None
        self._pos_vel_sub = None
        self._pose_sub = None
        self._update_timer = None
        self._vel_control = False

        self._pos_pub = None
        self._vel_pub = None
        self._pose_pub = None
        self._forward_kin = None
        self._inverse_kin = None

        self._reached_pub = None

    def simulate(self, forward_kin, inverse_kin):
        self._forward_kin = forward_kin
        self._inverse_kin = inverse_kin

        self._pos_sub = rospy.Subscriber('iiwa/command/JointPosition', JointPosition, self._pos_cmd_cb)
        self._vel_sub = rospy.Subscriber('iiwa/command/JointVelocity', JointVelocity, self._vel_cmd_cb)
        self._pos_vel_sub = rospy.Subscriber('iiwa/command/JointPositionVelocity', JointPositionVelocity, self._pos_vel_cmd_cb)
        self._pose_sub = rospy.Subscriber('iiwa/command/CartesianPose', PoseStamped, self._pose_cmd_cb)

        self._pos_pub = rospy.Publisher('/iiwa/state/JointPosition', JointPosition, queue_size=100)
        self._vel_pub = rospy.Publisher('/iiwa/state/JointVelocity', JointVelocity, queue_size=100)
        self._pose_pub = rospy.Publisher('/iiwa/state/CartesianPose', PoseStamped, queue_size=100)

        self._reached_pub = rospy.Publisher('/iiwa/state/DestinationReached', Time, queue_size=100)

        self._vel_control = False
        self._start_update_timer()

    def stop_simulation(self):
        self._pos_sub.unregister()
        self._vel_sub.unregister()
        self._pos_vel_sub.unregister()
        self._pose_sub.unregister()
        self._stop_update_timer()
        self._pos_sub = None
        self._vel_sub = None
        self._pos_vel_sub = None
        self._pose_sub = None
        self._reached_pub = None
        self._forward_kin = None
        self._inverse_kin = None
        self._vel_control = False

    def _start_update_timer(self, rate=200):
        if self._update_timer is None:
            self._update_timer = rospy.Timer(rospy.Duration(1.0/rate), self._update)

    def _stop_update_timer(self):
        if self._update_timer is not None:
            self._update_timer.shutdown()
            self._update_timer = None

    def _update(self, event):
        if self._vel_control:
            self._current_pos += self._current_vel * self._update_timer._period.to_sec()
            self.joint_pub.publish(JointState(header=header(), name=self.name, position=self._current_pos))

        self._pos_pub.publish(joint_pos_msg(*self._current_pos))
        self._vel_pub.publish(joint_vel_msg(*self._current_vel))
        self._pose_pub.publish(pose_msg(self._forward_kin(self._current_pos), frame_id='world'))

    def _pos_cmd_cb(self, msg):
        self._vel_control = False
        self.pub_joint_array(joint_pos_array(msg))

    def _vel_cmd_cb(self, msg):
        self._vel_control = True
        self._current_vel = joint_vel_array(msg)

    def _pos_vel_cmd_cb(self, msg):
        self.pub_joints(pos_vel_msg_to_joint_state(msg, self.name))

    def _pose_cmd_cb(self, msg):
        q = self._inverse_kin(pose_array(msg))
        if q is None:
            print('Pose unreachable!')
            return
        self.pub_joint_array(q)

    def pub_joints(self, msg):
        msg.header = header()
        self.joint_pub.publish(msg)
        self._current_pos = np.array(msg.position)
        self._reached_pub.publish(Time(data=rospy.Time.now()))

    def pub_joint_array(self, joints):
        self.pub_joints(JointState(header=header(), name=self.name, position=joints))

    def pub_path(self, msg):
        """

        :param Path msg:
        :return:
        """
        msg.header = header('world')
        self.path_pub.publish(msg)

    def pub_odom(self, msg):
        msg.header = header('world')

    def sim_trajectories(self, joints=None, poses=None, rate=100, show_path=False):
        if poses is not None:
            odom_msgs = poses_to_odom_msgs(poses, 1.0/rate)
            if show_path:
                path = odom_msgs_to_path_msg(odom_msgs)
                self.pub_path(path)

            if joints is not None:
                joint_msgs = joint_trajectory_to_joint_state_msgs(joints, self.name)

                self._trajectory = zip(joint_msgs, odom_msgs)
                self._trajectory_cmd = self.pub_joints_and_odom

            else:
                self._trajectory = odom_msgs
                self._trajectory_cmd = self.pub_odom

        else:
            joint_msgs = joint_trajectory_to_joint_state_msgs(joints, self.name)
            self._trajectory = joint_msgs
            self._trajectory_cmd = self.pub_joints

        self._timer = rospy.Timer(rospy.Duration(1.0/rate), self._trajectory_cb)

    def pub_joints_and_odom(self, msgs):
        joint_msg, odom_msg = msgs
        self.pub_joints(joint_msg)
        self.pub_odom(odom_msg)

    def _trajectory_cb(self, event):
        """
        Callback for running the currently saved trajectory in a separate thread
        """
        try:
            self._trajectory_cmd(self._trajectory.pop(0))
        except IndexError:
            self._timer.shutdown()
            self._timer = None
            self._trajectory = None
            self._trajectory_cmd = None