#!/usr/bin/python

# -*- coding: utf-8 -*-
import re
import sys

from pydocmd.__main__ import main

documented_classes = [
    'IIWASubscriber',
    'IIWACommander',
    'IIWAInterface'
]

sys_args = ['simple'] + ['iiwa_py.'+s+'++' for s in documented_classes]

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    for arg in sys_args:
        sys.argv.append(arg)
    sys.exit(main())