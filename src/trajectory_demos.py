#!/usr/bin/env python
import numpy as np
from iiwa_py import startup_iiwa_interface
import rospy


interface = startup_iiwa_interface('trajectory_demos')
interface.simulate_cmds()


interface.increment_pose(x=0.05, z=-0.3)
rospy.sleep(0.5)
p0 = interface.get_pose()
j0 = interface.get_pos()
pT = p0 + np.array([0.5, 0, 0, 0, 0, 0, 0])

rate = 100
T = 3.0
K = int(rate*T)

traj = [p0 + (pT-p0)/K*(i+1) for i in range(300)]

interface.pose_trajectory(traj, rate, start_joints=j0, biased=False)

rospy.sleep(0.5)
print('Ending')